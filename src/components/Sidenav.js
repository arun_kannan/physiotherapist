import { Sidenav,
        Nav,
        Icon,
        Avatar,
        Button,
    } from 'rsuite';

export default function SidenavBar(props) {

    return(
        <div className="customSidebar">
        <Sidenav defaultOpenKeys={['3', '4']} activeKey="1" className="customSidebar-nav">
          <Sidenav.Body>
            <Nav>
                {
                    props.userlist && props.userlist.findPatient ? props.userlist.findPatient.map((item, index) => {
                        return <Nav.Item eventKey={index} key={index} onClick={() => props.setActiveUser(item)} className={props.activeUser && props.activeUser.patientNumber === item.patientNumber ? 'active': ''}>
                                    <Avatar circle className="avatar">{ item.patientName.substr(0,2).toUpperCase() }</Avatar>
                                { item.patientName }                            
                            </Nav.Item>
                    } ) : null
                }
            </Nav>
          </Sidenav.Body>

          <Button onClick={props.updateShow} className="navFooter"> 
            <Icon icon="plus-circle" size="2x"></Icon>
              Add new user
          </Button>
        </Sidenav>
      </div>
    )
}