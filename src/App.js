import axios from "axios";
import { useEffect, useState } from "react";
import SidenavBar from './components/Sidenav';
import { Modal, Form, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button, Avatar, FlexboxGrid, Input, DatePicker, Divider, SelectPicker } from 'rsuite';
import FileBase64 from 'react-file-base64';

const data = [
  {
    "label": "1",
    "value": "1",
    "role": "1"
  },
  {
    "label": "2",
    "value": "2",
    "role": "2"
  },
  {
    "label": "3",
    "value": "3",
    "role": "3"
  }]

function App() {

  const [users, setUsers] = useState();
  const [show, setShow] = useState(false)
  const [newUser, setNewUser] = useState({})
  const [activeUser, setActiveUser] = useState()
  const [imgfiles, updateimgfiles] = useState()

  const [programmes, setProgrammes] = useState({
    activetab: 0,
    tabs: [
      {
        text: 'Exercise Programmes',
        count: 0,
        link: 'content1'
      },
      {
        text: 'Splinting Programmes',
        count: 0,
        link: 'content2'
      },{
        text: 'Standing Programmes',
        count: 0,
        link: 'content3'
      }
    ]
  })

  const [showProgramForm, setshowProgramForm] = useState(false)

  const [newProgram, setNewProgram] = useState({
    "patientNumber": 0,
    "exerciseProgram": {
      "nameExercise": "string",
      "exerciseCreatedDate": "2020-11-10T21:25:38.243Z",
      "goals": "string"
    },
    "exerciseList": [
      {
        "exerciseName": "string1",
        "reviewDate": "2020-11-10T21:25:38.243Z",
        "exerciseActivity": "string",
        "repetitionList": [
          {
            "reps": 0,
            "perWeek": 0,
            "timePeriod": 0
          },
          {
            "reps": 0,
            "perWeek": 0,
            "timePeriod": 0
          }
        ],
        "exerciseProgramImagesList": [
          {
            "fileName": "string",
            "fileUrl": "string"
          }
        ]
      }
    ]
  })

  useEffect(() => {
    getPatients()
  }, [])

  const getimgfiles = (files) => {
    updateimgfiles(files)
    uploadFiles(files)
  }

  const updateInputVal = (e, parent, child, isExcercise, repeatIndex) => {
    let data = newProgram
    if(!isExcercise) {
      data[parent][child] = e    
    } else if(isExcercise.toLowerCase() === 'excercise') {
      data[parent][0][child] = e
    } else {
      data[parent][0]['repetitionList'][repeatIndex][child] = e
    }
    setNewProgram(data)
  }
  
  const uploadFiles = async (files) => {
    let data = newProgram

    var dataPromise = new Promise((resolve, reject) => {
      data.exerciseList[0].exerciseProgramImagesList = []
      files.forEach(el => {
        let imgString = el.base64.split(',')[1]      
        if(imgString) {
          axios.post("http://bmosoftware-001-site5.itempurl.com/api/FileUpload/SaveImageFile", {
            "fileName": el.name,
            "folderName": "Program",
            "fileString": imgString
          }).then(res => {
              data.exerciseList[0].exerciseProgramImagesList.push(res.data.model.fileDetail)
          })
        }
      })
  });
  
  dataPromise.then(() => {
    setNewProgram(data)
  });
}

  const saveNewUser = () => {
    let data = newProgram
    data.patientNumber = activeUser.patientNumber
    axios.post("http://bmosoftware-001-site5.itempurl.com/api/ExerciseProgram/CreateExerciseProgram", data).then(res => {
      setShow(false)
      setshowProgramForm(false)
      getProgrammes(activeUser)
    })
  }

  const updateShow = () => {
    setShow(!show)
  }

  const updateValue = (item, target) => {
    let obj = newUser
    obj[target] = item
    setNewUser(obj)
  }
  
  const getPatients = () => {
    axios.get("http://bmosoftware-001-site5.itempurl.com/api/User/GetPatientDetails").then(res => {
      setUsers(res.data.model)
      setActiveUser(res.data.model.findPatient[0])
      getProgrammes(res.data.model.findPatient[0])
    })
  }

  const createNewUser = () => {    
    axios.post('http://bmosoftware-001-site5.itempurl.com/api/User/CreatePatient', {
      "patientName": newUser.name,
      "diagnosis": newUser.diagnosis
    }).then(() => {
      getPatients()
      updateShow()
    })
  }

  const selectUser = (e) => {
    setActiveUser(e)
    getProgrammes(e)
  }

  const getProgrammes = (user) => {
    axios.post("http://bmosoftware-001-site5.itempurl.com/api/ExerciseProgram/GetExerciseProgram", {
      "patientNumber": user.patientNumber
    }).then(res => {
      let obj = programmes
      setProgrammes(null)
      let content = res.data.model.exerciseProgramList
      obj['tabs'][obj.activetab].count = content.length
      obj.content = content
      setProgrammes(obj)
    })
  }

  // const createProgram = () => {
  // }

  const addRepetition = (item, index) => {
    let obj = newProgram
    setNewProgram(null)

    setTimeout(() => {
      obj.exerciseList[index]['repetitionList'].push({
        "reps": 0,
        "perWeek": 0,
        "timePeriod": 0
      })
      setNewProgram(obj)
    }, 1000)
  }

  return (
    <div className="App">
      <header>
        Physiotherapist (admin) module
      </header>
      <main>
        <SidenavBar userlist={users} updateShow={() => updateShow()} setActiveUser={(e) => selectUser(e)} activeUser={activeUser} />
        <div className="content-wrapper">

        <Modal show={show} onHide={() => setShow(!show)}>
          <Modal.Header>
              <Modal.Title>Add new user</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <FormComponent updateValue={updateValue} createNewUser={createNewUser} updateShow={updateShow} />
          </Modal.Body>
        </Modal>

        <Modal show={showProgramForm} onHide={() => setshowProgramForm(!showProgramForm)}>
          <Modal.Header>
              <Modal.Title>Add new Program</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <ProgramForm newProgram={newProgram} addRepetition={addRepetition} getimgfiles={getimgfiles} saveNewUser={saveNewUser} updateInputVal={updateInputVal} />
          </Modal.Body>
        </Modal>

          <div className="content-wrapper-header">
            <div className="content-wrapper-header-left">
              <div className="content-wrapper-header-left-avatar">
                <Avatar circle className="avatar" size="lg">{ activeUser && activeUser.patientName.substr(0,2).toUpperCase() }</Avatar>
              </div>
              <div className="content-wrapper-header-left-info">
                <h4 className="content-wrapper-header-left-info-title">{ activeUser && activeUser.patientName }</h4>
                <p>NHS Number: <span>{ activeUser && activeUser.patientNumber }</span></p>
                <p>Diagnosis: <span>{ activeUser && activeUser.diagnosis }</span></p>
              </div>
            </div>
            <div className="content-wrapper-header-right">
              <Button appearance="ghost" onClick={() => setshowProgramForm(!showProgramForm)}>Add New Program</Button>
            </div>
          </div>

          <div className="content-wrapper-body">
            <div className="customTabs">
              <div className="customTabs-header">
                {
                  programmes ? programmes.tabs.map((tab, index) => {
                    return <a href={'#'+tab.link} className={programmes.activetab == index ? 'active':''} key={index}>{tab.text} ({tab.count})</a>
                  }) : null
                }
              </div>
              
              <div className="customTabs-content">
                {
                  programmes && programmes.content ? programmes.content.map((item, index) => {
                    return <Program item={item} index={index} key={index} />
                  }):null
                }
              </div>

            </div>
          </div>

        </div>
      </main>
    </div>
  );
}

function ProgramForm(props) {
  return (
    <div className="ProgramForm">
      <FlexboxGrid>
      <FlexboxGrid.Item colspan={12}>
        { console.log(props.newProgram, "props.newProgram") }
        <label>Name of exercise programme</label>
        <Input type="text" placeholder="Name of exercise programme" onChange={(e) => props.updateInputVal(e, 'exerciseProgram', 'nameExercise')} />
      </FlexboxGrid.Item>
      <FlexboxGrid.Item colspan={12}>
        <label>Date</label>
        <DatePicker placeholder="Date" onChange={(e) => props.updateInputVal(e, 'exerciseProgram', 'exerciseCreatedDate')} />
      </FlexboxGrid.Item>
      <FlexboxGrid.Item colspan={24}>
        <label>Goals/objectives</label>
        <Input
          componentClass="textarea"
          rows={3}
          style={{resize: 'auto'}}
          onChange={(e) => props.updateInputVal(e, 'exerciseProgram', 'goals')}
        />
      </FlexboxGrid.Item>
    </FlexboxGrid>

    <Divider />
    {
      props.newProgram && props.newProgram.exerciseList.map((program, programIndex) => {
        return <FlexboxGrid key={programIndex}>
          <FlexboxGrid.Item colspan={12}>
            <label>Exercise Name</label>
            <Input type="text" placeholder="Exercise Name" onChange={(e) => props.updateInputVal(e, 'exerciseList', 'exerciseName', 'excercise')} />
          </FlexboxGrid.Item>
          <FlexboxGrid.Item colspan={12}>
            <label>Review date</label>
            <DatePicker placeholder="Review date" onChange={(e) => props.updateInputVal(e, 'exerciseList', 'reviewDate', 'excercise')} />
          </FlexboxGrid.Item>
          <FlexboxGrid.Item colspan={24}>
            <label>Description of the exercise activity</label>
            <Input
              componentClass="textarea"
              rows={3}
              style={{resize: 'auto'}}
              onChange={(e) => props.updateInputVal(e, 'exerciseList', 'exerciseActivity', 'excercise')}
            />
          </FlexboxGrid.Item>
            <FlexboxGrid.Item colspan={24}>
                <label>Initial</label>
            </FlexboxGrid.Item>
          {
            program.repetitionList && program.repetitionList.map((repet, repetindex) => {
              return <FlexboxGrid.Item colspan={24} key={repetindex}>
                <FlexboxGrid>
                  <FlexboxGrid.Item colspan={8}>
                    <label>Reps</label>
                    <Input type="text" placeholder="Reps" onChange={(e) => props.updateInputVal(e, 'exerciseList', 'reps', 'repetetion', repetindex)} />
                  </FlexboxGrid.Item>
                  <FlexboxGrid.Item colspan={8}>
                    <label>times per week</label>
                    {/* <Input type="text" placeholder="timesperweek" /> */}
                    <SelectPicker
                      data={data}
                      appearance="default"
                      placeholder="Default"
                      onChange={(e) => props.updateInputVal(e, 'exerciseList', 'perWeek', 'repetetion', repetindex)}
                    />
                  </FlexboxGrid.Item>
                  <FlexboxGrid.Item colspan={8}>
                    <label>for the first</label>
                    <Input type="text" placeholder="Time Period" onChange={(e) => props.updateInputVal(e, 'exerciseList', 'timePeriod', 'repetetion', repetindex)} />
                  </FlexboxGrid.Item>
                </FlexboxGrid>
              </FlexboxGrid.Item>
            })
          }
          <FlexboxGrid.Item colspan={24}>
            <Button appearance="subtle" onClick={() => props.addRepetition(program, programIndex)}>+ Add progression repetition</Button>
          </FlexboxGrid.Item>
          <FlexboxGrid.Item colspan={24}>            
            <FileBase64
              multiple={ true }
              onDone={ (e) => props.getimgfiles(e) } />
          </FlexboxGrid.Item>
        </FlexboxGrid>
      })
    }
    <Button appearance="ghost" block onClick={() => props.saveNewUser()}>Save</Button>
    </div>
  )
}

function Program(props) {
  return (
    <div className="program">
      <div className="program-header">
        <div className="program-header-left">
          <h4 className="program-header-left-title">{ props.item.nameExercise } <span>({ props.item.exerciseCreatedDate.substr(0, 10) })</span></h4>
          <p>{ props.item.goals }</p>
        </div>
        <div className="program-header-right">
          <h4 className="program-header-right-title">{ props.item.exerciseResultList && props.item.exerciseResultList.length } exercises</h4>
          { console.log(props.item, "props.item") }
          <Button appearance="ghost">Share with Parents</Button>
        </div>
      </div>
      <div className="program-content">        
        {
          props.item.exerciseResultList ? props.item.exerciseResultList.map((item, index) => {
            return <div className="customTable">
            <div className="customTable-inner">
              <div className="customTable-header">
                <div className="customTable-cell">#</div>
                <div className="customTable-cell">Name</div>
                <div className="customTable-cell">Description</div>
                <div className="customTable-cell">Repetitions</div>
                <div className="customTable-cell">Review Date</div>
              </div>
              <div className="customTable-group">
                <div className="customTable-cell"><b>1</b></div>
                <div className="customTable-cell"><b>{ item.exerciseName }</b></div>
                <div className="customTable-cell">{ item.exerciseActivity }</div>
                <div className="customTable-cell">
                  {/* { item.repetitionsList } */}
                  <table>
                    {
                      item.repetitionsList ? item.repetitionsList.map(repeatetion => {
                        return <tr >
                          <td><span>{ repeatetion.reps }</span> reps</td>
                          <td><span>{ repeatetion.perWeek }</span> time per week</td>
                          <td>for first <span>{ repeatetion.timePeriod } weeks</span></td>
                        </tr>
                      }):null
                    }
                  </table>
                </div>
                <div className="customTable-cell">{ item.reviewDate.substr(0,10) }</div>
              </div>
            </div>
            <div className="customTable-footer">
              {
                props.item.exerciseProgramImageList ? props.item.exerciseProgramImageList.map((imgsrc, imgIndex) => {
                  return <div className="customTable-footer-item">
                    <img src={imgsrc.fileUrl} />
                  </div>
                }):null
              }
              
              { console.log(props.item.exerciseProgramImageList, "props.item") }
            </div>
          </div>  
          }):null
        }
      </div>
    </div>
  )
}

function FormComponent(props) {
  return(
      <Form>
          <FormGroup>
          <ControlLabel>Patient name</ControlLabel>
          <FormControl name="name" onChange={e => props.updateValue(e, 'name')} />
          </FormGroup>
          <FormGroup>
          <ControlLabel>Diagnosis</ControlLabel>
          <FormControl name="diag" onChange={e => props.updateValue(e, 'diagnosis')} />
          </FormGroup>
          <FormGroup>
          <ButtonToolbar>
              <Button appearance="primary" onClick={() => props.createNewUser()}>Submit</Button>
              <Button appearance="default" onClick={() => props.updateShow()}>Cancel</Button>
          </ButtonToolbar>
          </FormGroup>
      </Form>
  )
}

export default App;
